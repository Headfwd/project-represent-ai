# Bring in deps
from dotenv import load_dotenv
import os

import streamlit as st 
from langchain.llms import OpenAI
from langchain.prompts import PromptTemplate
from langchain.chains import LLMChain 
from langchain.memory import ConversationBufferMemory

# Load environment variables
load_dotenv()

# Get the OpenAI API Key from the environment variables
os.environ['OPENAI_API_KEY'] = os.getenv("OPENAI_API_KEY")

# Set the page title
st.set_page_config(page_title="Represent AI", page_icon="🗣️🤖")

# App framework
st.title('🗣️🤖 Represent AI')
prompt = st.text_input('Write your prompt here') 

# Prompt templates
title_template = PromptTemplate(
    input_variables = ['topic'], 
    template='give me interesting information about this prompt {topic}'
)

# Memory 
title_memory = ConversationBufferMemory(input_key='topic', memory_key='chat_history')

# Llms
llm = OpenAI(temperature=0.9) 
title_chain = LLMChain(llm=llm, prompt=title_template, verbose=True, output_key='title', memory=title_memory)

# Show stuff to the screen if there's a prompt
if prompt: 
    title = title_chain.run(prompt)

    st.write(title) 
