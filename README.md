# represent-ai

Test for first commit

## Setup

```bash 
    pip install -r requirements.txt
```

or

```bash 
    python -m venv env
    source env/bin/activate  # On Windows use `env\Scripts\activate`
    pip install streamlit langchain openai wikipedia chromadb tiktokenizer python-dotenv
```

## Running the app

```bash
    streamlit run app.py
```

## Stop

```bash
    deactivate
```